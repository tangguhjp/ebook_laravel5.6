<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Users;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Users::create([
            'name'=>'tangguh',
            'username'=>'tangguh22',
            'email'=>'tang@tang.com',
            'password'=>Hash::make('123456789'),
            'role_id'=>1
        ]);
    }
}
