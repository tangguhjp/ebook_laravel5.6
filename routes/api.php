<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//USERS
Route::post('user/login', 'UserController@login');                              //login
Route::post('user/register', 'UserController@register');                        //register 
Route::post('user/logout', 'UserController@logout')->middleware('auth:api');    //logout
Route::post('user/update', 'UserController@update')->middleware('auth:api');    //update profile user

Route::get('user/', 'UserController@index');                                    //get all user
Route::get('user/{id}', 'UserController@user');                                 //get user by id
Route::get('user/delete/{id}', 'UserController@destroy');


Route::get('book/', 'BookController@index');
Route::get('book/{id}', 'BookController@book');
Route::post('book/upload', 'BookController@upload');
Route::post('book/download', 'BookController@download')->middleware('auth:api');
Route::post('book/edit', 'BookController@update');
Route::get('book/delete/{id}', 'BookController@destroy');


Route::get('comment/', 'CommentController@index');
Route::get('comment/{id}', 'CommentController@comment');
Route::post('comment/create', 'CommentController@create');
Route::post('comment/delete', 'CommentController@destroy')->middleware('auth:api');
Route::post('comment/edit/', 'CommentController@update')->middleware('auth:api');

 