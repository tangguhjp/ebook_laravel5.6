<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Users extends Authenticatable
{
    use HasApiTokens, Notifiable;
    //table name
    protected $table = 'users';
    protected $fillable = ['name','username','email', 'password','remember_token','role_id'];

    //primary key
    public $primaryKey = 'id';

    public $timestamps = true;
}
