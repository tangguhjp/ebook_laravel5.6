<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Books extends Authenticatable
{
    //table name
    protected $table = 'books';
    protected $fillable = ['title', 'author', 'publisher', 'year', 'category', 'description', 'file', 'cover'];    

    //primary key
    public $primaryKey = 'book_id';

    public $timestamps = true;
}
