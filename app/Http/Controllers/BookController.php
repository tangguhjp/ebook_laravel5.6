<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use App\Books;


class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Books::all();
    }

    public function book($id)
    {
        $book = Books::find($id);
        if(!empty($book)){
            $response['message']=true;
            $response['book']=$book;
        }else{
            $response['message']=false;
        }
        return $response;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function upload(Request $request)
    {
        $response['action']='upload';

        $data = $request->except(['file','cover']);
        
            
        $data['file'] = '';
        $data['cover'] = '';

        $id = Books::insertGetId($data);
        $data = Books::find($id);
            
        $fileName = $id.'.'.$request->file('file')->getClientOriginalExtension();
        $coverName = $id.'.'.$request->file('cover')->getClientOriginalExtension();
        
        $pdfPath = $request->file('file')->storeAs(
            'files/books', $fileName
        );
        $data['file'] = $pdfPath;

        $coverPath = $request->file('cover')->storeAs(
            'files/cover', $coverName
        );
        $data['cover'] = $coverPath;
        if($data->save()){
            $response['message']=true;
            $response['book']=$data;
        }else{
            $response['message']=false;
        }
        return $response;
    }

    public function download(Request $request)
    { 
       $path= storage_path('app/'.Books::find($request->id)->file);
       return response()->download($path); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $response['action']='update';
        $previousData = Books::find($request->id);
        $book = Books::find($request->id);
        
        $book->title = $request->title;
        $book->author = $request->author;
        $book->publisher = $request->publisher;
        $book->year = $request->year;
        $book->category = $request->category;
        $book->description = $request->description;        

        $this->validate($request, [
            'file' => 'nullable|file|max:2000'
        ]);
        
        
        if($request->hasFile('file')&&$request->hasFile('cover')){
            Storage::disk('local')->delete($previousData->file, $previousData->cover);    

            $fileName = $request->id.'.'.$request->file('file')->getClientOriginalExtension();
            $coverName = $request->id.'.'.$request->file('cover')->getClientOriginalExtension();  

            $file = $request->file('file')->storeAs(
                'files/books', $fileName
            );
            $cover = $request->file('cover')->storeAs(
                'files/cover', $coverName
            );

            $book->file = $file;
            $book->cover= $cover;
        }else if($request->hasFile('file')){
            Storage::disk('local')->delete($previousData->file);      
            $fileName = $request->id.'.'.$request->file('file')->getClientOriginalExtension(); 

            $file = $request->file('file')->storeAs(
                'files/books', $fileName
            );
            
            $book->file = $file;
        }else if($request->hasFile('cover')){
            Storage::disk('local')->delete($previousData->cover);      
            $coverName = $request->id.'.'.$request->file('cover')->getClientOriginalExtension();         
            
            $cover = $request->file('cover')->storeAs(
                'files/cover', $coverName
            );

            $book->cover= $cover;
        }

        if($book->save()){
            $response['message'] = true;
            $response['book'] = $book;         
        }else{
            $response['message'] = false;            
        }

        return $response;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $response['action']='delete';
        
        $book = Books::find($id);
        Storage::disk('local')->delete($book->file);
        Storage::disk('local')->delete($book->cover);
        
        if($book->delete()){
            $response['message']=true;
        }else{
            $response['message']=false;
        }
        return $response;
    }
}
