<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Comments;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Comments::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $response['action'] = 'create';

        $comment = $request->all();
        if(Comments::create($comment)){
            $response['message'] = true;
            $response['comment'] = $comment;
        }else{
            $request['message'] = false; 
        }
        return $response;
    }

    public function comment($id){
        $comment = Comments::find($id);
        if(!empty($comment)){
            $response['message']=true;
            $response['comment']=$comment;
        }else{
            $response['message']=false;
        }
        return $response;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $response['action'] = 'update';
        $comment = Comments::find($request->id);
        $comment->value = $request->value;
        if($comment->save()){
            $response['message'] = true;
            $response['comment'] = $comment;
        }else{
            $response['message'] = false;            
        }
        return $response;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request )
    {
        $response['action']='delete';
        
        if($comment=Comments::find($request['id'])->delete()){
            $response['message']=true;
        }else{
            $response['message']=false;
        }
        return $response;
    }
}
