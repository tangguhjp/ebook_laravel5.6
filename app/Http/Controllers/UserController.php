<?php

    namespace App\Http\Controllers;

    use Illuminate\Support\Facades\DB;
    use App\Http\Controllers\Controller;
    use Illuminate\Support\Facades\Hash;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\Session;
    use Illuminate\Support\Str;
    use Validator;
    

    use Illuminate\Http\Request;
    use App\Users;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $user = Users::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $response['action']='register';                
        $data = $request->all();
        $data['password'] = Hash::make($data['password']);
        if(Users::create($data)){
            $response['message']=true;
        }else{
            $response['message']=false;
        }
        return $response;
    }

    public function login(Request $request){
        $response['action']='login';                
        if(Auth::attempt(['username'=>$request->input('username'), 'password'=>$request->input('password')])){
            $user = $request->user();
            $response['message']=true;                        
            $response['token']=$user->createToken('XXX')->accessToken;
            $response['error']=false;
        }else{
            $response['message']=false;                       
            $response['error']=true;
        }
        return $response;
    }

    public function logout(Request $request){
        $response['action']='logout';        
        if(Auth::user()->token()->revoke() && Auth::user()->token()->delete() ){
            $response['message']=true;
        }else{
            $response['message']=false;            
        }
        return $response;
    }

    public function user($id){
        $user = Users::find($id);
        return $user;
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $response['action']='update';
        $data = $request->user();
        $data['name'] = $request->name;
        $data['username'] = $request->username;
        $data['password'] = Hash::make($request->password);
        $data['email'] = $request->email;
        if($data->save()){
            $response['message']=true;
        }else{          
            $response['message']=false; 
        }
        return $response;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if($user = Users::find($id)->delete()){
            $response['message']=true;
        }else{
            $response['message']=false;
        }
        return $response; 
    }
}
