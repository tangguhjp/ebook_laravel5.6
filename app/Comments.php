<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{
    //table name
    protected $table = 'comments';
    protected $fillable = ['value', 'book_id', 'username'];    

    //primary key
    public $primaryKey = 'id';

    public $timestamps = true;
}
